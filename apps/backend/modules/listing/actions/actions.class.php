<?php

require_once dirname(__FILE__) . '/../lib/listingGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/listingGeneratorHelper.class.php';

/**
 * listing actions.
 *
 * @package    board
 * @subpackage listing
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class listingActions extends autoListingActions {

    /**
     * Overrides auto-generated edit function to extend functionality
     * 
     * @param sfWebRequest $request
     */
    public function executeEdit(sfWebRequest $request) {
        
        // Gets images info of the lsiting
        $this->board_image = Doctrine_Core::getTable('BoardImage')->getImagesByListingId($request->getParameter('id'));
        $this->board_listing = $this->getRoute()->getObject();
        $this->form = $this->configuration->getForm($this->board_listing);
    }

    /**
     * Removes the image from the database
     * 
     * @param sfWebRequest $request
     */
    public function executeRemoveImage(sfWebRequest $request) {
        $board_image = Doctrine_Core::getTable('BoardImage')->find(array($request->getParameter('image_id')));
        $board_image->delete();
        $this->redirect($this->generateUrl('listing_edit', array('id' => $request->getParameter('id'))));
    }

}
