<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
    
    <?php foreach($board_image as $board_image):?>
<img src="<?php echo $board_image->getWebPath(); ?>">

        <form action="<?php echo url_for('listing/removeImage'); ?>" method="post">
            <input type="hidden" name="image_id" value="<?php echo $board_image->id; ?>">
            <input type="hidden" name="id" value="<?php echo $board_image->listing_id; ?>">
            <input class="form-control" type="submit" name="edit-submit" value="Delete"/>
        </form>
        <br />
<?php endforeach; ?>
  <?php echo form_tag_for($form, '@board_listing') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('listing/form_fieldset', array('board_listing' => $board_listing, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('listing/form_actions', array('board_listing' => $board_listing, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
