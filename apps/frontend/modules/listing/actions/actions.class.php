<?php

/**
 * listing actions.
 *
 * @package    board
 * @subpackage listing
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class listingActions extends sfActions {

    /**
     * Displays the index page
     *
     * @param sfWebRequest $request
     */
    public function executeIndex(sfWebRequest $request) {

        //Gets information about all categories to display category pages
        $this->board_category = Doctrine_Core::getTable('BoardCategory')->findAll();

        // Builds the page with the listings provided
        $this->pager = new sfDoctrinePager('BoardListing', sfConfig::get('app_max_listings_on_homepage'));
        $this->pager->setQuery(Doctrine_Core::getTable('BoardListing')->getListings());

        // Sets current user to the logged in user
        if ($this->getUser()->getGuardUser()) {
            $this->current_user = $this->getUser()->getGuardUser()->getId();
        }
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    /**
     * Displays the listing
     *
     * @param sfWebRequest $request
     */
    public function executeShow(sfWebRequest $request) {
        // Sets current user to the logged in user
        if ($this->getUser()->getGuardUser()) {
            $this->current_user = $this->getUser()->getGuardUser()->getId();
        }

        // Gets the listing info from the Model
        $this->board_listing = Doctrine_Core::getTable('BoardListing')->find(array($request->getParameter('id')));
        $this->board_image = Doctrine_Core::getTable('BoardImage')->getImagesByListingId($request->getParameter('id'));

        // Renders the page if the listing exists
        $this->forward404Unless($this->board_listing);
    }

    /**
     * Renders the listing add page
     *
     * @param sfWebRequest $request
     */
    public function executeNew(sfWebRequest $request) {
        $this->form = new BoardListingForm();
        $this->form->setDefault('user_id', $this->getUser()->getGuardUser()->getId());
    }

    /**
     * Creates a new listing
     *
     * @param sfWebRequest $request
     */
    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

//        $message = $this->getMailer()->compose();
//        $message->setSubject('Ad posted');
//        $message->setTo($this->getUser()->getGuardUser()->getEmailAddress());
//        $message->setFrom('admin@board.local');
//        $message->setBody(
//                '<html>' .
//                ' <head></head>' .
//                ' <body>' .
//                '  Here is an image <img src="http://www.myfootballfacts.com/8681.jpg" alt="Image" />' .
//                ' <p>our ad hast been created successfully.</p>' .
//                ' </body>' .
//                '</html>', 'text/html' // Mark the content-type as HTML
//        );
//
//        $this->getMailer()->send($message);

        $this->form = new BoardListingForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    /**
     * Displays edit listing page
     *
     * @param sfWebRequest $request
     */
    public function executeEdit(sfWebRequest $request) {
        // throws mistake if the listing does not exist
        $this->forward404Unless($board_listing = Doctrine_Core::getTable('BoardListing')->find(array($request->getParameter('id'))), sprintf('Object board_listing does not exist (%s).', $request->getParameter('id')));

        // throws mistake if the user tries to edit a listing that does not belong to him
        $this->forward404Unless($board_listing['user_id'] == $this->getUser()->getGuardUser()->getId());

        $this->board_image = Doctrine_Core::getTable('BoardImage')->getImagesByListingId($request->getParameter('id'));

        // renders the listing form
        $this->form = new BoardListingForm($board_listing);
    }

    /**
     * Updates the listing info with the info from edit
     *
     * @param sfWebRequest $request
     */
    public function executeUpdate(sfWebRequest $request) {
        // Checks if the request method is POST or PUT
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));

        // Checks if the listing exists and retrieves it info
        $this->forward404Unless($board_listing = Doctrine_Core::getTable('BoardListing')->find(array($request->getParameter('id'))), sprintf('Object board_listing does not exist (%s).', $request->getParameter('id')));

        // sets the info
        $this->form = new BoardListingForm($board_listing);

        // Checks if the user that has updated the info is the owner of the listing
        $this->forward404Unless($board_listing['user_id'] == $this->getUser()->getGuardUser()->getId());

        // Updates the listing
        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    /**
     * Deletes listing
     *
     * @param sfWebRequest $request
     */
    public function executeDelete(sfWebRequest $request) {
        // Checks if the listing exists and retrieves it info
        $this->forward404Unless($board_listing = Doctrine_Core::getTable('BoardListing')->find(array($request->getParameter('id'))), sprintf('Object board_listing does not exist (%s).', $request->getParameter('id')));

        // Checks if the user that has updated the info is the owner of the listing
        $this->forward404Unless($board_listing['user_id'] == $this->getUser()->getGuardUser()->getId());

        // Deletes the listing
        $board_listing->delete();

        // Redirects to the index page
        $this->redirect('listing/index');
    }

    /**
     * Removes the image from the database and redirects back to the listing
     * 
     * @param sfWebRequest $request
     */
    public function executeRemoveImage(sfWebRequest $request) {
        $board_image = Doctrine_Core::getTable('BoardImage')->find(array($request->getParameter('image_id')));
        $board_image->delete();
        $this->redirect($this->generateUrl('listing_edit', array('id' => $request->getParameter('id'))));
    }

    /**
     * Processes the form
     *
     * @param sfWebRequest $request
     * @param sfForm $form
     */
    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $board_listing = $form->save();

            $this->redirect('listing/index');
        }
    }

    /**
     * Shows user's listings
     * 
     * @param sfWebRequest $request
     */
    public function executeShowUserListings(sfWebRequest $request) {

        // Checks if the current user is equal to the route ID
        if ($this->getUser()->getGuardUser()->getId() != $this->getRoute()->getObject()->getId()) {
            $this->redirect('listing/index');
        }

        // Builds the page with the listings provided
        $this->pager = new sfDoctrinePager('BoardListing', sfConfig::get('app_max_listings_on_homepage'));
        $this->pager->setQuery(Doctrine_Core::getTable('BoardListing')->getListings($this->getRoute()->getObject()->getId()));
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    /**
     * Displays listings related to the category
     */
    public function executeCategoryListings() {
        $this->pager = new sfDoctrinePager('BoardListing', sfConfig::get('app_max_listings_on_homepage'));
        $this->pager->setQuery(Doctrine_Core::getTable('BoardListing')->getListings(null, $this->getRoute()->getObject()->getId()));
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    /**
     * Performes the search
     * 
     * @param sfWebRequest $request
     */
    public function executeSearch(sfWebRequest $request) {
        $this->forwardUnless($query = $request->getParameter('query'), 'listing', 'index');

        //  $this->listing = Doctrine_Core::getTable('BoardListing')->getForLuceneQuery($query);
        $this->pager = new sfDoctrinePager('BoardListing', sfConfig::get('app_max_listings_on_homepage'));

//        var_dump(Doctrine_Core::getTable('BoardListing')->getForLuceneQuery($query)->getSql()); exit;

        $this->pager->setQuery(Doctrine_Core::getTable('BoardListing')->getForLuceneQuery($query));
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

}
