<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div class="row">
    <div class="col-md-4">
        <form role="form" action="<?php echo url_for('listing/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

            <?php if (!$form->getObject()->isNew()): ?>
                <input type="hidden" name="sf_method" value="put" />
            <?php endif; ?>

            <div class="form-group">
                <?php echo $form['category_id']->renderLabel(); ?>
                <?php echo $form['category_id']->render(array('class' => 'form-control')); ?>            </div>
            <div class="form-group">

                <?php echo $form['heading']->renderLabel(); ?>
                 <?php echo $form['heading']->renderError(); ?>
                <?php echo $form['heading']->render(array('class' => 'form-control')); ?>
            </div>  
            <div class="form-group"> 
                 <?php echo $form['description']->renderLabel(); ?>
                <?php echo $form['description']->renderError(); ?>
            <?php echo $form['description']->render(array('class' => 'form-control')); ?>
                </div>  
                <div class="form-group"> 
                 <?php echo $form['price']->renderLabel(); ?>
            <?php echo $form['price']->render(array('class' => 'form-control')); ?>
                </div>
            <?php echo $form->renderHiddenFields() ?>
               <div class="form-group"> 
                   <label for="images"> Images </label>          
                       <?php foreach ($form['newImages'] as $photo): ?>
                <?php echo $photo['name']->render(); ?>
            <?php endforeach; ?>
                       
               </div>
            <?php if (!$form->getObject()->isNew()): ?>
                &nbsp;<?php echo link_to('Delete', 'listing/delete?id=' . $form->getObject()->getId(), array('method' => 'delete')) ?>
            <?php endif; ?>

            <input class="form-control" type="submit" value="Save" />
        </form>
    </div>
</div>