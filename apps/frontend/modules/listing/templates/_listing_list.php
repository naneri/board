<table class="table">
    <thead>
    <tr>
        <th>Category</th>
        <th>Heading</th>
        <th>Description</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($pager->getResults() as $board_listing): ?>
        <tr>
            <td><?php echo $board_listing->getCategoryName() ?></td>
            <td><a href="<?php echo url_for('listing/show?id='.$board_listing->getId()) ?>"><?php echo $board_listing->getHeading() ?></a></td>
            <td><?php echo $board_listing->getDescription() ?></td>
            <td><?php echo $board_listing->getPrice() ?></td>
        <?php if (isset($current_user) and $current_user == $board_listing['user_id']): ?>
            <td><a href="<?php echo url_for('listing/edit?id='.$board_listing->getId()) ?>">Edit</a></td>
            <td><a href="<?php echo url_for('listing/delete?id='.$board_listing->getId()) ?>">Delete</a></td>
        <?php endif ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php if ($pager->haveToPaginate()): ?>
<div class="">
    <ul class="pagination">
        <li><a href="?page=1">
            Start
        </a></li> 
        
        <li><a href="?page=<?php echo $pager->getPreviousPage() ?>">
            <
        </a></li>

        <?php foreach ($pager->getLinks() as $page): ?>
            <?php if ($page == $pager->getPage()): ?>
        <li><a style="color:black" href="#"><?php echo $page ?></a></li>
            <?php else: ?>
        <li> <a href="?page=<?php echo $page ?>"><?php echo $page ?></a></li> 
            <?php endif; ?>
        <?php endforeach; ?>

        <li><a href="?page=<?php echo $pager->getNextPage() ?>">
           >
        </a></li>

        <li> <a href="?page=<?php echo $pager->getLastPage() ?>">
            End
        </a></li>
    </ul>
    </div>
<?php endif; ?>

<br />