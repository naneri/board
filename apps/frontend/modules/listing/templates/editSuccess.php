<?php if ($sf_user->isAuthenticated()): ?> 
<?php slot('add_listing') ?>
<li><a href='<?php echo url_for('@show_user_listings?id=' . $current_user); ?>'>Your listings</a></li>
<?php end_slot() ?>    
<?php endif ?>  
<div class="row">
<div class="col-md-4">
<h1>Edit Board listing</h1>

<?php foreach($board_image as $board_image):?>
<img src="<?php echo $board_image->getWebPath(); ?>">

        <form action="<?php echo url_for('listing/removeImage'); ?>" method="post">
            <input type="hidden" name="image_id" value="<?php echo $board_image->id; ?>">
            <input type="hidden" name="id" value="<?php echo $board_image->listing_id; ?>">
            <input class="form-control" type="submit" name="edit-submit" value="Delete"/>
        </form>
        <br />
<?php endforeach; ?>

<?php include_partial('form', array('form' => $form)) ?>
</div>
    </div>
