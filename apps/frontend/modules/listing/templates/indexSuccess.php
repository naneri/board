<?php if ($sf_user->isAuthenticated()): ?> 
    <?php slot('add_listing') ?>
    <li><a href='<?php echo url_for('@show_user_listings?id=' . $current_user); ?>'>Your listings</a></li>
    <?php end_slot() ?>    
<?php endif ?>  


    

    <h1 class="text-center">Latest Listings</h1>

    <?php include_partial('listing/listing_list', array('pager' => $pager)); ?>

<br />
<br />
Categories:<br />
    <?php foreach ($board_category as $board_category): ?>
        <a href="<?php echo url_for('@category_listings?id=' . $board_category->getId()); ?>"><?php echo $board_category->getName(); ?></a>
    <?php endforeach; ?>
