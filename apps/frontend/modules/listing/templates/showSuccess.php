<div class="row">
<div class="col-md-4">
<h3 class="text-center"><?php echo $board_listing->getHeading() ?></h3>
<?php foreach($board_image as $board_image):?>
<img class="thumbnail" src="<?php echo $board_image->getWebPath(); ?>">
<?php endforeach; ?>
<table>
  <tbody>
    <tr>
      <th>Category:</th>
      <td><?php echo $board_listing->getCategoryId() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $board_listing->getUserId() ?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?php echo $board_listing->getDescription() ?></td>
    </tr>
    <tr>
      <th>Price:</th>
      <td><?php echo $board_listing->getPrice() ?></td>
    </tr>
  </tbody>
</table>
</div>
    </div>
<hr />

<?php if (isset($current_user) and $current_user == $board_listing['user_id']): ?>
    <a href="<?php echo url_for('listing/edit?id='.$board_listing->getId()) ?>">Edit</a>
    <a href="<?php echo url_for('listing/delete?id='.$board_listing->getId()) ?>">Delete</a>
<?php endif ?>
    
