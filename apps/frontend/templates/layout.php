<!doctype HTML>
<html>
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"></script>
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>
        <title>
            Welcome to the board ad website
        </title>
    </head>
    <body>


        <div class="navbar navbar-default navbar-static-top">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="<?php echo url_for('listing/index') ?>">Main page</a></li>
            </ul> 
            <ul class="nav navbar-nav navbar-right">
                <?php if (!($sf_user->isAuthenticated())): ?>
                    <li> <a href="<?php echo url_for('sf_guard_register'); ?>">Register</a></li>
                    <li> <a href="<?php echo url_for('sf_guard_signin'); ?>">Login</a></li>
                <?php else: ?>
                    <li><a href="<?php echo url_for('listing/new'); ?>">+Add listing</a>   </li> 
                    <li><a href='<?php echo url_for('@show_user_listings?id=' . $sf_user->getGuardUser()->getId()); ?>'>Your listings</a></li> 
                    <li><a href="<?php echo url_for('sf_guard_signout'); ?>">Logout</a></li>
                <?php endif ?>  
            </ul>
        </div>    

        <div class="container">
            <div class="search col-md-4"></div>
            <div class="search col-md-4">
                <form action="<?php echo url_for('listing_search') ?>" method="get" class="form-inline" role="form">
                    <div class="form-group ">

                        <input class="form-control" type="text" name="query" value="<?php echo $sf_request->getParameter('query') ?>" id="search_keywords" />
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
            <div class="search col-md-4"></div>
        </div>

        <div id="content">
            <?php if ($sf_user->hasFlash('notice')): ?>
                <div class="flash_notice">
                    <?php echo $sf_user->getFlash('notice') ?>
                </div>
            <?php endif ?>

            <?php if ($sf_user->hasFlash('error')): ?>
                <div class="flash_error">
                    <?php echo $sf_user->getFlash('error') ?>
                </div>
            <?php endif ?>
        </div>

        <?php echo $sf_content ?>
        <div class="footer">
            <div class="text-center">The website was developed by "insert name here"</div>
        </div>   
    </body>
</html>
