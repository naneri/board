<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($board_listing->getId(), 'board_listing_edit', $board_listing) ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_category_id">
  <?php echo $board_listing->getCategoryId() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_user_id">
  <?php echo $board_listing->getUserId() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_heading">
  <?php echo $board_listing->getHeading() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_description">
  <?php echo $board_listing->getDescription() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_price">
  <?php echo $board_listing->getPrice() ?>
</td>
