<?php use_helper('I18N', 'Date') ?>
<?php include_partial('listing/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('New Listing', array(), 'messages') ?></h1>

  <?php include_partial('listing/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('listing/form_header', array('board_listing' => $board_listing, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('listing/form', array('board_listing' => $board_listing, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('listing/form_footer', array('board_listing' => $board_listing, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
