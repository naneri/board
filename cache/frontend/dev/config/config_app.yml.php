<?php
// auto-generated by sfDefineEnvironmentConfigHandler
// date: 2014/02/26 15:24:52
sfConfig::add(array(
  'app_max_listings_on_homepage' => 5,
  'app_products_upload_path' => 'uploads/',
  'app_sf_guard_plugin_success_signout_url' => '/frontend_dev.php',
));
