<?php
// auto-generated by sfSecurityConfigHandler
// date: 2014/02/27 13:07:53
$this->security = array (
  'index' => 
  array (
    'is_secure' => false,
  ),
  'show' => 
  array (
    'is_secure' => false,
  ),
  'categorylistings' => 
  array (
    'is_secure' => false,
  ),
  'search' => 
  array (
    'is_secure' => false,
  ),
  'all' => 
  array (
    'is_secure' => true,
  ),
  'secure' => 
  array (
    'is_secure' => false,
  ),
  'signin' => 
  array (
    'is_secure' => false,
  ),
  'signout' => 
  array (
    'is_secure' => false,
  ),
);
