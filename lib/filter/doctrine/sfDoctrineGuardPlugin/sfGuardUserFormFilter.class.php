<?php

/**
 * sfGuardUser filter form.
 *
 * @package    board
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserFormFilter extends PluginsfGuardUserFormFilter
{
  public function configure()
  {
      unset($this['created_at'], $this['updated_at'], $this['expires_at'], $this['is_activated'], $this['last_login'], $this['groups_list'], $this['permissions_list'], $this['salt'], $this['password'], $this['algorithm']);
  }
}
