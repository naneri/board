<?php

/**
 * BoardImage form.
 *
 * @package    board
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BoardImageForm extends BaseBoardImageForm
{
 
  public function configure()
  {
      $this->useFields(array('name'));

      $this->setWidget('name', new sfWidgetFormInputFile());
      $this->setValidator('name', new sfValidatorFile(array(
          'mime_types' => 'web_images',
          'path' => sfConfig::get('sf_upload_dir'),
      )));
      
      $this->setValidator('name', new sfValidatorFile(array(
    'mime_types' => 'web_images',
    'path' => sfConfig::get('sf_upload_dir'),
    'required' => false,
  )));
 
  }
}
