<?php

/**
 * BoardListing form.
 *
 * @package    board
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BoardListingForm extends BaseBoardListingForm {

    public function configure() {
        unset(
                $this['created_at'], $this['updated_at'], $this['expires_at'], $this['is_activated']
        );


        $this->widgetSchema['user_id'] = new sfWidgetFormInputHidden();

        $subForm = new sfForm();
        for ($i = 0; $i < 2; $i++) {
            $boardImage = new BoardImage();
            $boardImage->BoardListing = $this->getObject();

            $form = new BoardImageForm($boardImage);

            $subForm->embedForm($i, $form);
        }
        $this->embedForm('newImages', $subForm);
    }

    public function saveEmbeddedForms($con = null, $forms = null) {
        if (null === $forms) {
            $photos = $this->getValue('newImages');
            $forms = $this->embeddedForms;
            
            foreach ($this->embeddedForms['newImages'] as $name => $form) {
                if (!isset($photos[$name]) || $photos[$name]['name'] == null) {
                    unset($forms['newImages'][$name]);
                }
            }
        }

        return parent::saveEmbeddedForms($con, $forms);
    }

}
