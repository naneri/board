<?php

/**
 * BoardImageTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class BoardImageTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object BoardImageTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('BoardImage');
    }
    
    public function getImagesByListingId($listing_id){
        
        $q = $this->createQuery('c')
            ->where('c.listing_id = '. $listing_id);

        return $q->execute();
        
    }
}